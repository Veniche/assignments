package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        this.npm = npm;
        this.nama = nama;
        this.komponenPenilaian = komponenPenilaian;
        // Note: komponenPenilaian merupakan skema penilaian yang didapat dari GlasDOS.
    }

    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        KomponenPenilaian a = null;
        for (int i = 0; i < this.komponenPenilaian.length; i++){
                if (this.komponenPenilaian[i].getNama().equals(namaKomponen)){
                    a = this.komponenPenilaian[i];
                }
        }
        // Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.
        return a;
    }

    public String getNpm() {
        return this.npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A" :
            nilai >= 80 ? "A-" :
            nilai >= 75 ? "B+" :
            nilai >= 70 ? "B" :
            nilai >= 65 ? "B-" :
            nilai >= 60 ? "C+" :
            nilai >= 55 ? "C" :
            nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    public String rekap() {
        String hasil = "\n";
        double nilaiAkhir = 0.0;
        for (int i = 0; i < this.komponenPenilaian.length; i++){
            hasil += this.komponenPenilaian[i].toString() + "\n";
            nilaiAkhir += this.komponenPenilaian[i].getNilai();
        }
        hasil += "Nilai akhir: " + String.format("%.2f", nilaiAkhir) + "\nHuruf: " + getHuruf(nilaiAkhir) + "\n" + getKelulusan(nilaiAkhir);
        return hasil;
    }

    public String toString() {
        return this.npm + " - " + this.nama;
    }

    public String getDetail() {
        String hasil = "";
        double nilaiAkhir = 0.0;
        for (int i = 0; i < this.komponenPenilaian.length; i++){
            if (this.komponenPenilaian[i] != null) {
                hasil += this.komponenPenilaian[i].getDetail() + "\n";
                nilaiAkhir += this.komponenPenilaian[i].getNilai();
            } else {
                return null;
            }
        }
        hasil += "\n" + "Nilai akhir: " + String.format("%.2f", nilaiAkhir) + "\nHuruf: " + getHuruf(nilaiAkhir) + "\n" + getKelulusan(nilaiAkhir);
        return hasil;
    }

    @Override
    public int compareTo(Mahasiswa other) {
        // Hint: bandingkan NPM-nya, String juga punya method compareTo.
        return this.npm.compareTo(other.npm);
    }
}
