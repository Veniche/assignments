package assignments.assignment2;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class AsistenDosen {
    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    public AsistenDosen(String kode, String nama) {
        this.kode = kode;
        this.nama = nama;
    }

    public String getKode() {
        return this.kode;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa.add(mahasiswa);
        Collections.sort(this.mahasiswa);
        // Hint: kamu boleh menggunakan Collections.sort atau melakukan sorting manual.
        // Note: manfaatkan method compareTo pada Mahasiswa.
    }

    public Mahasiswa getMahasiswa(String npm) {
        for (int i = 0; i < this.mahasiswa.size(); i++){
            if (this.mahasiswa.get(i).getNpm().equals(npm)){
                return this.mahasiswa.get(i);
            }
        }
        // Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.
        return null;
    }

    public String rekap() {
        String a = "";
        for (int i = 0; i < this.mahasiswa.size(); i++){
            a += this.mahasiswa.get(i).toString();
            a += this.mahasiswa.get(i).rekap() + "\n";
        }
        return a;
    }

    public String toString() {
        return this.kode + " - " + this.nama;
    }
}
