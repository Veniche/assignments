package assignments.assignment2;

public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;

    public ButirPenilaian(double nilai, boolean terlambat) {
        this.terlambat = terlambat;
        if (nilai < 0) {
            this.nilai = 0;
        } else if (this.terlambat) {
            this.nilai = nilai - ((nilai * PENALTI_KETERLAMBATAN) / 100);
        } else {
            this.nilai = nilai;
        }        
    }

    public double getNilai() {
        return this.nilai;
    }

    @Override
    public String toString() {
        String hasil = String.format("%.2f", this.nilai);
        if (this.terlambat){
            hasil = hasil + " (T)";
        }
        return hasil;
    }
}
