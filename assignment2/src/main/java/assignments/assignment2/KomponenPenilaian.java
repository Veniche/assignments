package assignments.assignment2;

public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        this.nama = nama;
        this.bobot = bobot;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
        // Note: banyakButirPenilaian digunakan untuk menentukan panjang butirPenilaian saja
        // (tanpa membuat objek-objek ButirPenilaian-nya).
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        this.butirPenilaian[idx] = butir;
    }

    public String getNama() {
        return this.nama;
    }

    public double getRerata() {
        double count = 0.00;
        int counter = 0;
        if (this.butirPenilaian.length != 0){
            for (int i = 0; i < this.butirPenilaian.length; i++){
                if (this.butirPenilaian[i] != null){
                    count += this.butirPenilaian[i].getNilai();
                    counter ++;
                }
            }
        } 
        if (counter == 0) {
            return 0;
        }
        double rerata = count / counter;
        return rerata;
    }

    public double getNilai() {
        double rerata = getRerata();
        double nilai = (this.bobot * rerata) / 100;
        return nilai;
    }

    public String getDetail() {
        String hasil = "~~~ " + this.nama + " (" + this.bobot +  "%) ~~~";
        if (this.butirPenilaian.length > 1) {
            int counter = 1;
            for (int i = 0; i < butirPenilaian.length; i++) {
                if ( butirPenilaian[i] != null) {
                    hasil += ("\n" + this.nama + " " + counter + ": " + butirPenilaian[i].toString());
                }
                counter ++;
            }
            hasil += "\n" + "Rerata: " + String.format("%.2f", this.getRerata());
            hasil += "\n" + "Kontribusi nilai akhir: " + String.format("%.2f", this.getNilai());
            return hasil;
        }
        if (this.butirPenilaian[0] != null) {
            hasil += "\n" + this.nama + ": " + this.butirPenilaian[0].toString() + "\n" + "Kontribusi nilai akhir: " + String.format("%.2f", this.butirPenilaian[0].getNilai() * this.bobot / 100);
        }
        if (this.butirPenilaian[0] == null) {
            hasil += "\n" + this.nama + ": " + "0.00" + "\n" + "Kontribusi nilai akhir: " + "0.00";
        }
        return hasil;
    }

    @Override
    public String toString() {
        String hasil = String.format("Rerata %s: %.2f", this.nama, getRerata());
        return hasil;
    }

}
