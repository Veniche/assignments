package assignments.assignment1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;

    /** 
     * Ini encoder.
     * @param data encode
     * @return hasil
     */
    public static String encode(String data) {
        int m = data.length();
        int r = 1;
        // Mencari jumlah parity bits
        while (Math.pow(2, r) < (m + r + 1)) {
            r++;
        }
        int[] ar = insertParity(data, m, r);
        int[] var = checkParity(ar, r); 
        String hasil = "";
        int[] vvar = new int[var.length - 1];
        for (int i = 1; i < var.length; i++) {
            vvar[i - 1] = var[i];
        }
        // Mengubah array var menjadi string hasil
        for (int i : vvar) {
            hasil += i;
        }
        return hasil;
    }

    /** 
     * Insert a parity.
     * @param str data
     * @param m data length
     * @param r parity 
     */
    // Memasukkan parity bit pada posisinya, default parity = 0
    public static int[] insertParity(String str, int m, int r) {
        int[] ar = new int[r + m + 1];
        int j = 0;
        for (int i = 1; i < ar.length; i++) {
            if ((Math.ceil(Math.log(i) / Math.log(2)) 
                - Math.floor(Math.log(i) / Math.log(2))) == 0) {
                ar[i] = 0;
            } else {
                char a = str.charAt(j);
                int b = Character.getNumericValue(a);
                ar[i] = b;
                j++;
            }
        }
        return ar;
    }
    /**
     * Check parity.
     * @param ar arrays
     * @param r parity 
     */
    // Mengecek nilai parity bit 0 atau 1

    public static int[] checkParity(int[] ar, int r) {
        for (int i = 0; i < r; i++) {
            int count = 0;
            int s = (int) Math.pow(2, i + 1);
            int x = (int) Math.pow(2, i);
            for (int j = x; j < ar.length; j += s) {
                for (int k = j; k < j + Math.pow(2, i); k++) {
                    if (k >= ar.length) {
                        break;
                    } else {
                        if (ar[k] == 1) {
                            count++;
                        }
                    }
                }
            }
            // Mengubah parity bit yang salah menjadi 1
            if (count % 2 == 1) {
                ar[x] = 1;
            }
        }
        return ar;
    }
    /**
     * Ini decoder.
     * @param code decode 
     */

    public static String decode(String code) {
        int m = code.length();
        int r = 0;
        // Mencari jumlah parity bit
        while (Math.pow(2, r) < m) {
            if (m != 1 & m != 2) {
                r++;
            }
        }
        // Membuat array yang berisi bit total
        int[] ar = new int[m];
        for (int i = 0; i < code.length(); i++) {
            char a = code.charAt(i);
            int b = Character.getNumericValue(a);
            ar[i] = b;
        }

        int[] rar = errorDetector(ar, r);
        ArrayList<Integer> ray = removeParity(m, r, rar);
        String hasil = "";
        // Mengubah arraylist menjadi string
        for (int s : ray) {
            hasil += s;
        }
        return hasil;
    }
    /** Error detector.
     * @param ar arrays
     * @param r parity 
     */
    // Mencari error dan memperbaikinya

    public static int[] errorDetector(int[] ar, int r) {
        int b = 0;
        int par2 = 0;
        for (int i = 0; i < r; i++) {
            int paritycheck = 0;
            int skip = (int) Math.pow(2, i + 1);
            int indexparity = (int) Math.pow(2, i) - 1;
            for (int j = indexparity; j < ar.length; j += skip) {
                for (int k = j; k < j + Math.pow(2, i); k++) {
                    if (k >= ar.length) {
                        break;
                    } else {
                        if (ar[k] == 1) {
                            paritycheck++;
                        }
                    }
                }
            }
            if (paritycheck % 2 == 1) {
                b += (indexparity + 1);
                par2++;
            }
        }
        if (par2 != 0) {
            if (ar[b - 1] == 0) {
                ar[b - 1] = 1;
            } else {
                ar[b - 1] = 0;
            }
        }
        return ar;
    }
    /** Remove parity.
     * @param m data length
     * @param r parity
     * @param ar arrays 
     */
    // Mengeluarkan parity bits

    public static ArrayList<Integer> removeParity(int m, int r, int[] ar) {
        // Mengubah array menjadi arraylist untuk mempermudah remove
        ArrayList<Integer> nar = new ArrayList<Integer>(m);
        for (int i : ar) {
            nar.add(i);
        }
        // Arraylist berisi index parity bits
        ArrayList<Integer> index = new ArrayList<Integer>(r);
        for (int i = 0; i < r; i++) {
            int x = (int) Math.pow(2, i) - 1;
            index.add(x);
        }
        filter(nar, index);
        return nar;
    }
    /** Filter parity.
     * @param list arraylist data
     * @param indexesToRemove arraylist index 
     */
    // Meng-filter parity dari arraylist
    
    public static void filter(ArrayList<Integer> list, ArrayList<Integer> indexesToRemove) {
        Collections.reverse(indexesToRemove);
        for (Integer indexToRemove : indexesToRemove) {
            list.remove((int) indexToRemove);
        }
    }

    /**
     * Main program for Hamming Code.
     * 
     * @param args main
     */
    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            } else {
                System.out.println("Nomor operasi salah!");
            }
        }
        in.close();
    }
}