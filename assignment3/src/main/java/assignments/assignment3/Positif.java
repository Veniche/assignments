package assignments.assignment3;

public class Positif implements Status{
  
    public String getStatus(){
        return "Positif";
    }

    public void tularkan(Carrier penular, Carrier tertular){
        // Implementasikan apabila object Penular melakukan interaksi dengan object tertular
        // Handle kasus ketika keduanya benda dan manusia menularkan benda
        if (penular.getTipe().equalsIgnoreCase("Benda") & tertular.getTipe().equalsIgnoreCase("Benda")) {
            return;
        } else if (penular.getTipe().equalsIgnoreCase("Manusia") & tertular.getTipe().equalsIgnoreCase("Benda")) {
            if (tertular instanceof Benda) {
                ((Benda) tertular).tambahPersentase();
            }
            if (tertular.getStatusCovid().equalsIgnoreCase("Positif")) {
                tertular.ubahStatus("Positif");
                tertular.tambahrantaiinteraksi(penular);
                penular.tambahtotal();
            }
        } else {
            tertular.ubahStatus("Positif");
            tertular.tambahrantaiinteraksi(penular);
            penular.tambahtotal();
        }
    }
}