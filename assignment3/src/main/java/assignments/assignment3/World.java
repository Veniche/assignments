package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World{

    public List<Carrier> listCarrier;

    public World(){
        this.listCarrier = new ArrayList<Carrier>();
    }

    public Carrier createObject(String tipe, String nama){
        // Implementasikan apabila ingin membuat object sesuai dengan parameter yang diberikan
        if (tipe.equalsIgnoreCase("Cleaning_Service")) {
            Carrier a = new CleaningService(nama);
            this.listCarrier.add(a);
            return a;

        } else if (tipe.equalsIgnoreCase("Petugas_Medis")) {
            Carrier a = new PetugasMedis(nama);
            this.listCarrier.add(a);
            return a;

        } else if (tipe.equalsIgnoreCase("Ojol")) {
            Carrier a = new Ojol(nama);
            this.listCarrier.add(a);
            return a;

        } else if (tipe.equalsIgnoreCase("Jurnalis")) {
            Carrier a = new Jurnalis(nama);
            this.listCarrier.add(a);
            return a;

        } else if (tipe.equalsIgnoreCase("Pekerja_Jasa")) {
            Carrier a = new PekerjaJasa(nama);
            this.listCarrier.add(a);
            return a;

        } else if (tipe.equalsIgnoreCase("Pintu")) {
            Carrier a = new Pintu(nama);
            this.listCarrier.add(a);
            return a;

        } else if (tipe.equalsIgnoreCase("Pegangan_Tangga")) {
            Carrier a = new PeganganTangga(nama);
            this.listCarrier.add(a);
            return a;

        } else if (tipe.equalsIgnoreCase("Angkutan_Umum")) {
            Carrier a = new AngkutanUmum(nama);
            this.listCarrier.add(a);
            return a;
            
        } else if (tipe.equalsIgnoreCase("Tombol_Lift")) {
            Carrier a = new TombolLift(nama);
            this.listCarrier.add(a);
            return a;
        }
        return null;
    }

    public Carrier getCarrier(String nama){
        for (Carrier i : listCarrier) {
            if (i.getNama().equals(nama)) {
                return i;
            }
        }
        return null;
    }
}
