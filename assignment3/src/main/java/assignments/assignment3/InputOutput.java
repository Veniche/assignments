package assignments.assignment3;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class InputOutput{
  	
    private BufferedReader br;
    private PrintWriter pw;
    private String inputFile;
    private String outputFile; 
    private World world;

    public InputOutput(String inputType, String inputFile, String outputType, String outputFile)
            throws IOException {
        // Buat constructor untuk InputOutput
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        setBufferedReader(inputType);
        setPrintWriter(outputType);
    }

    public void setBufferedReader(String inputType) throws FileNotFoundException {
        // Membuat BufferedReader bergantung inputType (I/O text atau input terminal)
        if (inputType.equalsIgnoreCase("TERMINAL")) {
            this.br = new BufferedReader(new InputStreamReader(System.in));
        } else if (inputType.equalsIgnoreCase("TEXT")) {
            this.br = new BufferedReader(new FileReader(this.inputFile));
        }
    }
    
    public void setPrintWriter(String outputType) throws IOException {
        // Membuat PrintWriter bergantung inputType (I/O text atau output terminal) 
        if (outputType.equalsIgnoreCase("TERMINAL")) {
            this.pw = new PrintWriter(System.out);
        } else if (outputType.equalsIgnoreCase("TEXT")) {
            this.pw = new PrintWriter(new File(this.outputFile));
        }
    }

    public void belumtertular(Carrier a) throws BelumTertularException {
        if (a.getStatusCovid().equals("Negatif")) {
            throw new BelumTertularException(a.toString() + "berstatus negatif");
        }
    }

    public void run() throws IOException{
        // Program utama untuk InputOutput, jangan lupa handle untuk IOException
        // Buatlah objek dengan class World
        // Untuk membuat object Carrier baru dapat gunakan method CreateObject pada class World
        // Untuk mengambil object Carrier dengan nama yang sesua dapat gunakan method getCarrier pada class World
        
        List<String> masuk = new ArrayList<>();
        Boolean tanda = true;
        while (tanda.equals(true)) {
            String y = this.br.readLine();
            masuk.add(y);
            if (y.equalsIgnoreCase("EXIT")) {
                tanda = false;
            }
        }
        this.world = new World();
        this.br.close();
        
        for (String baris : masuk) {
            String[] kata = baris.split(" ");

            if (kata[0].equalsIgnoreCase("ADD")) {
                // Add object dan status default negatif
                this.world.createObject(kata[1], kata[2]);
                this.world.getCarrier(kata[2]).ubahStatus("Negatif");

            } else if (kata[0].equalsIgnoreCase("INTERAKSI")) {
                this.world.getCarrier(kata[2]).interaksi(this.world.getCarrier(kata[1]));

            } else if (kata[0].equalsIgnoreCase("POSITIFKAN")) {
                // Ubah status menjadi positif dan tambahkan rantai
                this.world.getCarrier(kata[1]).ubahStatus("Positif");
                this.world.getCarrier(kata[1]).tambahrantai();

            } else if (kata[0].equalsIgnoreCase("SEMBUHKAN")) {
                PetugasMedis a = (PetugasMedis) this.world.getCarrier(kata[1]);
                if (this.world.getCarrier(kata[2]).getTipe().equalsIgnoreCase("MANUSIA")) {
                    Manusia x = (Manusia) this.world.getCarrier(kata[2]);
                    a.obati(x);
                }

            } else if (kata[0].equalsIgnoreCase("BERSIHKAN")) {
                CleaningService a = (CleaningService) this.world.getCarrier(kata[1]);
                if (this.world.getCarrier(kata[2]).getTipe().equalsIgnoreCase("Benda")) {
                    Benda x = (Benda) this.world.getCarrier(kata[2]);
                    a.bersihkan(x);
                }

            } else if (kata[0].equalsIgnoreCase("RANTAI")) {
                // Buat rantai dari object, jika belum tertular masuk ke BelumTertularException
                try {
                    belumtertular(this.world.getCarrier(kata[1]));
                    String a = "Rantai Penyebaran " + this.world.getCarrier(kata[1]).toString() + ": ";
                    List<Carrier> la = this.world.getCarrier(kata[1]).getRantaiPenular();
                    for (Carrier i : la) {
                        if (i.getNama().equals(kata[1])) {
                            a += i.toString();
                            break;
                        } else {
                            a += i.toString(); 
                            a += " -> ";
                        }
                    }
                    this.pw.println(a);

                } catch (BelumTertularException e) {
                    this.pw.println("assignments.assignment3.BelumTertularException: " + this.world.getCarrier(kata[1]).toString() + " berstatus negatif");;
                    e.getMessage();
                }

            } else if (kata[0].equalsIgnoreCase("TOTAL_KASUS_DARI_OBJEK")) {
                this.pw.println(this.world.getCarrier(kata[1]).toString() + " telah menyebarkan virus COVID ke " + 
                this.world.getCarrier(kata[1]).getTotalKasusDisebabkan() + " objek");
            
            } else if (kata[0].equalsIgnoreCase("AKTIF_KASUS_DARI_OBJEK")) {
                this.pw.println(this.world.getCarrier(kata[1]).toString() + " telah menyebarkan virus COVID dan masih teridentifikasi positif sebanyak " + 
                this.world.getCarrier(kata[1]).getAktifKasusDisebabkan() + " objek");
            
            } else if (kata[0].equalsIgnoreCase("TOTAL_SEMBUH_MANUSIA")) {
                this.pw.println("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: " + Manusia.getJumlahSembuh() + " kasus"); 
            
            } else if (kata[0].equalsIgnoreCase("TOTAL_SEMBUH_PETUGAS_MEDIS")) {
                PetugasMedis a = (PetugasMedis) this.world.getCarrier(kata[1]);
                this.pw.println(this.world.getCarrier(kata[1]).toString() + " menyembuhkan " + a.getJumlahDisembuhkan() + " manusia");
            
            } else if (kata[0].equalsIgnoreCase("TOTAL_BERSIH_CLEANING_SERVICE")) {
                CleaningService a = (CleaningService) this.world.getCarrier(kata[1]);
                this.pw.println(this.world.getCarrier(kata[1]).toString() + " membersihkan " + a.getJumlahDibersihkan() + " benda");
            
            } else if (kata[0].equalsIgnoreCase("EXIT")) {
                continue;
            }
        }

        this.pw.close();
    }
    
}