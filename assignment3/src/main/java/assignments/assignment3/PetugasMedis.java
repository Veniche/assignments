package assignments.assignment3;

public class PetugasMedis extends Manusia{
  		
    private int jumlahDisembuhkan;

    public PetugasMedis(String nama){
        //Buat constructor untuk Petugas Medis
        super(nama);
    }

    public void obati(Manusia manusia) {
        // Implementasikan apabila objek PetugasMedis ini menyembuhkan manusia
        // Update nilai atribut jumlahDisembuhkan
        manusia.ubahStatus("Negatif");
        manusia.resetrantai();
        this.jumlahDisembuhkan += 1;
        this.tambahSembuh();
    }

    public int getJumlahDisembuhkan(){
        return this.jumlahDisembuhkan;
    }

    @Override
    public String toString() {
        return "PETUGAS MEDIS " + this.getNama();
    }

}