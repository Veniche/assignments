package assignments.assignment3;

public class AngkutanUmum extends Benda{

    public AngkutanUmum(String name){
        //Buat constructor untuk Angkutan Umum
        super(name);
    }

    @Override
    public void tambahPersentase() {
        //Tambah persentase sesuai yang ditentukan
        //Jika sudah 100 atau lebih, benda positif
        this.persentaseMenular += 35;
        if (this.persentaseMenular >= 100) {
            this.ubahStatus("Positif");
        }
    }

    @Override
    public String toString() {
        return "ANGKUTAN UMUM " + this.getNama();
    }
}