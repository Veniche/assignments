package assignments.assignment3;

public class BelumTertularException extends Exception	{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public BelumTertularException(String errorMessage) {
		super(errorMessage);
	}
}