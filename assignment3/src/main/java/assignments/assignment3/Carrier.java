package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public abstract class Carrier{

    private String nama;
    private String tipe;
    private Status statusCovid;
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular;

    public Carrier(String nama,String tipe){
        this.nama = nama;
        this.tipe = tipe;
        this.rantaiPenular = new ArrayList<>();
    }

    public String getNama(){
        return this.nama;
    }

    public String getTipe(){
        return this.tipe;
    }

    public String getStatusCovid(){
        return this.statusCovid.getStatus();
    }

    public int getAktifKasusDisebabkan(){
        return this.aktifKasusDisebabkan;
    }

    public int getTotalKasusDisebabkan(){
        return this.totalKasusDisebabkan;
    }

    public List<Carrier> getRantaiPenular(){
        return this.rantaiPenular;
    }

    public void ubahStatus(String status){
        // Method untuk mengubah status
        if (status.equalsIgnoreCase("Positif")) {
            this.statusCovid = new Positif();
        } else if (status.equalsIgnoreCase("Negatif")) {
            this.statusCovid = new Negatif();
        }
    }

    public void interaksi(Carrier lain){
        // Implementasikan jika object ini berinteraksi dengan object lain
        // Cek status kedua object terlebih dahulu
        if (this.getStatusCovid().equalsIgnoreCase("Positif")) {
            if (!(lain.getStatusCovid().equalsIgnoreCase("Positif"))) {
                this.statusCovid.tularkan(this, lain);
                if (lain.getStatusCovid().equalsIgnoreCase("Positif")) {
                    this.totalKasusDisebabkan += 1;
                    this.aktifKasusDisebabkan += 1;
                }
            }
        } else if (lain.getStatusCovid().equalsIgnoreCase("Positif")) {
            if (!(this.getStatusCovid().equalsIgnoreCase("Positif"))) {
                lain.statusCovid.tularkan(lain, this);
                if (this.getStatusCovid().equalsIgnoreCase("Positif")) {
                    lain.totalKasusDisebabkan += 1;
                    lain.aktifKasusDisebabkan += 1;
                }
            }
        }
    }

    public void tambahtotal() {
        // Method untuk menambah Total Kasus Disebabkan secara tidak langsung
        for (Carrier i : this.rantaiPenular) {
            if (i.nama.equals(this.nama)) {
                break;
            } else {
                i.totalKasusDisebabkan += 1;
                i.aktifKasusDisebabkan += 1;
            }
        }
    }

    public void cekaktif() {
        // Method untuk mengurangi object yang sudah sembuh di Aktif Kasus Disebabkan
        for (Carrier i : this.rantaiPenular) {
            if (i.nama.equals(this.nama)) {
                break;
            } else {
                i.aktifKasusDisebabkan -= 1;
            }
        }
    }

    public void tambahrantai() {
        // Membuat Rantai Penular baru untuk object ini
        this.rantaiPenular = new ArrayList<Carrier>();
        this.rantaiPenular.add(this);
    }

    public void tambahrantaiinteraksi(Carrier sumber) {
        // Menambah Rantai Penular saat terjadi penularan secara tidak langsung
        this.rantaiPenular = new ArrayList<Carrier>();
        for (Carrier i : sumber.rantaiPenular) {
            this.rantaiPenular.add(i);
        }
        this.rantaiPenular.add(this);
    }

    public void resetrantai() {
        // Rantai direset jika object disembuhkan
        this.cekaktif();
        this.rantaiPenular = new ArrayList<Carrier>();
        this.rantaiPenular.add(this);
    }

    public abstract String toString();

}
