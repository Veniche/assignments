package assignments.assignment3;

public abstract class Manusia extends Carrier{
    private static int jumlahSembuh = 0;
    
    public Manusia(String nama){
        //Buat constructor Manusia
        super(nama, "Manusia");
    }
    
    public void tambahSembuh(){
        // Fungsi untuk menambahkan nilai pada atribut jumlahSembuh.
        jumlahSembuh += 1;
    }

    public static int getJumlahSembuh() {
        return jumlahSembuh;
    }
    
}