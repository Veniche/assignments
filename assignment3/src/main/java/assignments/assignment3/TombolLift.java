package assignments.assignment3;

public class TombolLift extends Benda{
      
    public TombolLift(String name){
        //Buat constructor untuk Tombol Lift
        super(name);
    }

    @Override
    public void tambahPersentase() {
        //Tambah persentase sesuai yang ditentukan
        //Jika sudah 100 atau lebih, benda positif
        this.persentaseMenular += 20;
        if (this.persentaseMenular >= 100) {
            this.ubahStatus("Positif");
        }
    }

    @Override
    public String toString() {
        return "TOMBOL LIFT " + this.getNama();
    }
}