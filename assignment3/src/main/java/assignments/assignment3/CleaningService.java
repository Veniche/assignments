package assignments.assignment3;

public class CleaningService extends Manusia{
  		
    private int jumlahDibersihkan;

    public CleaningService(String nama){
        //Buat constructor untuk Cleaning Service
        super(nama);
    }

    public void bersihkan(Benda benda){
        // Implementasikan apabila objek CleaningService ini membersihkan benda
        // Update nilai atribut jumlahDibersihkan
        benda.ubahStatus("Negatif");
        benda.resetrantai();
        benda.persentaseMenular = 0;
        this.jumlahDibersihkan += 1;
    }

    public int getJumlahDibersihkan(){
        return this.jumlahDibersihkan;
    }

    @Override
    public String toString() {
        return "CLEANING SERVICE " + this.getNama();
    }

}