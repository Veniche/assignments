package assignments.assignment3;

public class PekerjaJasa extends Manusia{
  	
    public PekerjaJasa(String nama){
        //Buat constructor untuk Pekerja Jasa
    	super(nama);
    }

    @Override
    public String toString() {
        return "PEKERJA JASA " + this.getNama();
    }
  	
}