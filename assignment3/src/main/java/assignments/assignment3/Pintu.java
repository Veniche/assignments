package assignments.assignment3;

public class Pintu extends Benda{
    
    public Pintu(String name){
        //Buat constructor untuk Pintu
        super(name);
    }

    public void tambahPersentase() {
        //Tambah persentase sesuai yang ditentukan
        //Jika sudah 100 atau lebih, benda positif
        this.persentaseMenular += 30;
        if (this.persentaseMenular >= 100) {
            this.ubahStatus("Positif");
        }
    }

    @Override
    public String toString() {
        return "PINTU " + this.getNama();
    }
}