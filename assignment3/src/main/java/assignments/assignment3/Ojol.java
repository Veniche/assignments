package assignments.assignment3;

public class Ojol extends Manusia{
  	
    public Ojol(String name){
        //Buat constructor untuk Ojol
		super(name);
    }

    @Override
    public String toString() {
        return "OJOL " + this.getNama();
    }
  	
}